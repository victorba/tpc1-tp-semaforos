#include "exception/exception.hpp"
#include "model/pizzeria.hpp"
#include <iostream>

int main(int argc, char *argv[]) {
  try {
    Pizzeria pizzeria;
    pizzeria.run(argc, argv);
  } catch (const std::runtime_error &e) {
    std::cout << e.what() << std::endl;
  }

  return 0;
}
