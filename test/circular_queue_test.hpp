#ifndef __CIRCULAR_QUEUE_TEST_HPP__
#define __CIRCULAR_QUEUE_TEST_HPP__

#include "communication/circular_queue.hpp"

class CircularQueueTest : public CxxTest::TestSuite {
public:
  void test_should_init_empty() {
    CircularQueue<int> q;
    TS_ASSERT(q.is_empty());
  }

  void test_should_not_init_full() {
    CircularQueue<int> q;
    TS_ASSERT(!q.is_full());
  }

  void test_should_init_with_size_zero() {
    CircularQueue<int> q;
    TS_ASSERT_EQUALS(0, q.size());
  }

  void test_should_after_enqueueing_item() {
    CircularQueue<int> q;
    q.enqueue(99);
    TS_ASSERT_EQUALS(1, q.size());
  }

  void test_should_decrease_after_dequeueing_item() {
    CircularQueue<int> q;
    q.enqueue(99);
    q.enqueue(88);
    q.dequeue();
    TS_ASSERT_EQUALS(1, q.size());
  }

  void test_should_return_dequeued_item() {
    CircularQueue<int> q;
    q.enqueue(99);
    TS_ASSERT_EQUALS(99, q.dequeue());
  }

  void test_should_get_full_when_enqueueud_max_size_items() {
    CircularQueue<int> q;
    for (unsigned int i = 0; i < CircularQueue<int>::capacity(); i++) {
      q.enqueue(i);
    }
    TS_ASSERT(q.is_full());
  }

  void test_should_return_capacity() {
    TS_ASSERT_EQUALS(CQ_MAX_SIZE, CircularQueue<int>::capacity());
  }

  void test_enqueing_when_full_should_raise_exception() {
    CircularQueue<int> q;
    for (unsigned int i = 0; i < CircularQueue<int>::capacity(); i++) {
      q.enqueue(i);
    }
    TS_ASSERT_THROWS(q.enqueue(99), std::runtime_error);
  }

  void test_dequeue_when_empty_raises_exception() {
    CircularQueue<int> q;
    TS_ASSERT_THROWS(q.dequeue(), std::runtime_error);
  }
};

#endif
