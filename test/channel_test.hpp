#ifndef __CHANNEL_TEST_HPP__
#define __CHANNEL_TEST_HPP__

#include "communication/channel.hpp"

class ChannelTest : public CxxTest::TestSuite {
public:
  void test_should_send_and_receive_a_number() {
    Channel<int> channel("/bin/bash");
    channel.send(1);
    TS_ASSERT_EQUALS(1, channel.recv());
    channel.free();
  }
};

#endif
