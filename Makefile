TESTS := $(wildcard ./test/*.hpp)
STATICLIB := ./build/liblibsemaphores.a
FLAGS := -std=c++11 -Werror -pedantic -Wall -O0 -ggdb
INC := ./include

fmt:
	find ./include/ -name "*.hpp" | sed 's| |\\ |g' | xargs clang-format -i
	find ./src/ -name "*.cpp" | sed 's| |\\ |g' | xargs clang-format -i
	find ./test/ -name "*.hpp" | sed 's| |\\ |g' | xargs clang-format -i
	find ./app/ -name "*.cpp" | sed 's| |\\ |g' | xargs clang-format -i

build:
	cd build && make

test:
	cd test && $(RM) runner.cpp && $(RM) runner
	make build
	cxxtestgen --error-printer -o ./test/runner.cpp $(TESTS)
	g++ -o ./test/runner ./test/runner.cpp $(FLAGS) -I$CXXTEST  -I$(INC) $(STATICLIB)

valgrind:
	valgrind --leak-check=full --show-leak-kinds=all --trace-children=yes --track-fds=yes --track-origins=yes $(TARGET)

.PHONY: fmt test valgrind build
