#include "exception/exception.hpp"
#include <sys/types.h>
#include <unistd.h>

Exception::Exception(const std::string &message)
    : std::runtime_error(this->format(message)) {}

std::string Exception::format(const std::string &message) const {
  std::stringstream ss;
  ss << message << " in " << getpid() << " : " << strerror(errno);
  return ss.str();
}
