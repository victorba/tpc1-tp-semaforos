#include "communication/semaphore.hpp"
#include "exception/exception.hpp"

Semaphore::Semaphore(const std::string &pathname, int projid, int count) {
  key_t key = ftok(pathname.c_str(), projid);
  if (key == -1) {
    throw Exception("Error Semaphore in ftok");
  }

  this->semid = semget(key, 1, 0666 | IPC_CREAT);
  if (this->semid == -1) {
    throw Exception("Error Semaphore in semget");
  }

  this->initialize(count);
}

Semaphore::~Semaphore() {}

int Semaphore::acquire() const {
  struct sembuf sops;
  sops.sem_num = 0;
  sops.sem_op = -1;
  sops.sem_flg = SEM_UNDO;
  int r = semop(this->semid, &sops, 1);
  if (r == -1) {
    throw Exception("Error Semaphore in acquire");
  }
  return r;
}

int Semaphore::release() const {
  struct sembuf sops;
  sops.sem_num = 0;
  sops.sem_op = 1;
  sops.sem_flg = SEM_UNDO;
  int r = semop(this->semid, &sops, 1);
  if (r == -1) {
    throw Exception("Error Semaphore in release");
  }
  return r;
}

void Semaphore::remove() const { semctl(this->semid, 0, IPC_RMID); }

void Semaphore::initialize(int count) const {
  union semnum {
    int val;
    struct semid_ds *buf;
    ushort *array;
  };

  semnum init;
  init.val = count;
  if (semctl(this->semid, 0, SETVAL, init) == -1) {
    throw Exception("Error Semaphore in initialize");
  }
}
