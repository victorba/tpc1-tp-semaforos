#include "model/shop/pizza.hpp"
#include <stdlib.h>
#include <unistd.h>

Pizza::Pizza(int id) : id(id), state(PIZZA_STATE::EMPTY) {}

Pizza::Pizza(int id, PIZZA_STATE state) : id(id), state(state) {}

int Pizza::get_id() const { return this->id; }

PIZZA_STATE Pizza::get_state() const { return this->state; }

void Pizza::add_dough() { state = PIZZA_STATE::ONLY_DOUGH; }

void Pizza::add_ingredients() { state = PIZZA_STATE::SEASONED; }

void Pizza::add_cheese() { state = PIZZA_STATE::RAW; }

int Pizza::bake() {
  int miliseconds = rand() % 1000;
  int microseconds = miliseconds * 1000;
  usleep(microseconds);
  state = PIZZA_STATE::BAKED;
  return miliseconds;
}
