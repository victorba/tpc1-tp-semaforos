#include "model/shop/pizza_serializer.hpp"

void PizzaSerializer::into(const Pizza &pizza, Packet &packet) {
  packet.body.id = pizza.get_id();
  packet.body.state = (int)pizza.get_state();
}

Pizza PizzaSerializer::from(int id, int state) {
  Pizza pizza(id, (PIZZA_STATE)state);
  return pizza;
}

Pizza PizzaSerializer::from_order(Packet &packet) {
  Pizza pizza(packet.body.id);
  return pizza;
}
