#include "model/pizzeria_protocol.hpp"
#include "model/shop/pizza_serializer.hpp"
#include "model/work/bake_pizza.hpp"
#include "model/work/cut_ingredients.hpp"
#include "model/work/grate_cheese.hpp"
#include "model/work/prepare_dough.hpp"
#include "model/work/put_in_oven.hpp"
#include "worker/work_serializer.hpp"

Pizza PizzeriaProtocol::from(Packet packet) {
  Pizza pizza = PizzaSerializer::from(packet.body.id, packet.body.state);
  return pizza;
}

Pizza PizzeriaProtocol::from_order(Packet packet) {
  Pizza pizza = PizzaSerializer::from_order(packet);
  return pizza;
}

Packet PizzeriaProtocol::into_prepare_dough(int id) {
  WorkSerializer *work_serializer = WorkSerializer::get_instance();
  Packet packet;
  packet.header.work = work_serializer->into(PrepareDough::get_instance());
  packet.body.id = id;
  return packet;
}

Packet PizzeriaProtocol::into_cut_ingredients(const Pizza &pizza) {
  return PizzeriaProtocol::into_packet(pizza, CutIngredients::get_instance());
}

Packet PizzeriaProtocol::into_grate_cheese(const Pizza &pizza) {
  return PizzeriaProtocol::into_packet(pizza, GrateCheese::get_instance());
}

Packet PizzeriaProtocol::into_put_in_oven(const Pizza &pizza) {
  return PizzeriaProtocol::into_packet(pizza, PutInOven::get_instance());
}

Packet PizzeriaProtocol::into_bake_pizza(const Pizza &pizza) {
  return PizzeriaProtocol::into_packet(pizza, BakePizza::get_instance());
}

Packet PizzeriaProtocol::into_packet(const Pizza &pizza, Work *work) {
  Packet packet;
  WorkSerializer *work_serializer = WorkSerializer::get_instance();
  packet.header.work = work_serializer->into(work);
  PizzaSerializer::into(pizza, packet);
  return packet;
}
