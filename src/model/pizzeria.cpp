#include "model/pizzeria.hpp"
#include "communication/shared_memory.hpp"
#include "exception/exception.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/work/bake_pizza.hpp"
#include "model/work/build_chamber.hpp"
#include "model/work/build_cutting_tool.hpp"
#include "model/work/build_dough_chef.hpp"
#include "model/work/build_grater.hpp"
#include "model/work/build_oven.hpp"
#include "model/work/cut_ingredients.hpp"
#include "model/work/grate_cheese.hpp"
#include "model/work/prepare_dough.hpp"
#include "model/work/put_in_oven.hpp"
#include "model/work/stop.hpp"
#include "worker/spawner.hpp"
#include "worker/work_serializer.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>

Pizzeria::Pizzeria() { srand(time(NULL)); }

void Pizzeria::run(int argc, char *argv[]) const {
  std::cout << "Pizzeria (parent process) in " << getpid() << std::endl;
  int orders = this->get_orders(argc, argv);
  this->load_works();

  WorkerHandle handle = WorkerSpawner::spawn("dough_chef.txt");
  if (handle.is_parent()) {
    Packet build = WorkerProtocol::into(BuildDoughChef::get_instance());
    handle.get_channel()->send(build);

    for (int i = 0; i < orders; i++) {
      Packet prepare = PizzeriaProtocol::into_prepare_dough(i);
      handle.get_channel()->send(prepare);
    }

    Packet stop;
    stop = WorkerProtocol::into(Stop::get_instance());
    handle.get_channel()->send(stop);
    handle.wait();
    handle.remove();
  }
  std::cout << "Exiting worker in " << getpid() << std::endl;
}

int Pizzeria::get_orders(int argc, char *argv[]) const {
  if (argc != this->ORDERS_INDEX + 1) {
    std::runtime_error e("Missing orders. Use it as => ./pizzeria <orders>.");
    throw e;
  }
  return std::stoi(argv[this->ORDERS_INDEX]);
}

void Pizzeria::load_works() const {
  WorkSerializer *ws = WorkSerializer::get_instance();
  ws->add(Stop::get_instance());
  ws->add(BuildDoughChef::get_instance());
  ws->add(BuildCuttingTool::get_instance());
  ws->add(BuildGrater::get_instance());
  ws->add(BuildOven::get_instance());
  ws->add(BuildChamber::get_instance());
  ws->add(PrepareDough::get_instance());
  ws->add(CutIngredients::get_instance());
  ws->add(GrateCheese::get_instance());
  ws->add(PutInOven::get_instance());
  ws->add(BakePizza::get_instance());
}
