#include "model/work/build_oven.hpp"
#include "communication/packet.hpp"
#include "model/work/build_chamber.hpp"
#include "model/work/build_grater.hpp"
#include "model/work/stop.hpp"
#include "unused.hpp"
#include "worker/spawner.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>
#include <sstream>

BuildOven BuildOven::instance;

Work *BuildOven::get_instance() { return &BuildOven::instance; }

void BuildOven::run(Worker *worker, Packet packet) {
  UNUSED(packet);
  std::cout << "Building oven in " << getpid() << std::endl;

  worker->get_integers()["chamber"] = 0;
  worker->get_integers()["MAX CHAMBERS"] = this->MAX_CHAMBERS;

  for (int i = 0; i < this->MAX_CHAMBERS; i++) {
    std::stringstream pathname;
    pathname << "chamber" << i << ".txt";
    WorkerHandle handle = WorkerSpawner::spawn(pathname.str());
    if (handle.is_parent()) {
      worker->get_handles().push_back(handle);
      Packet build = WorkerProtocol::into(BuildChamber::get_instance());
      handle.get_channel()->send(build);
    } else {
      this->free_handles(worker, handle);
      worker->stop();
      break;
    }
  }
}

void BuildOven::free_handles(Worker *worker, WorkerHandle &handle) const {
  for (auto other_handle : worker->get_handles()) {
    if (other_handle.getpid() != handle.getpid()) {
      other_handle.get_channel()->free();
    }
  }
}
