#include "model/work/stop.hpp"
#include "communication/packet.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

Stop Stop::instance;

Work *Stop::get_instance() { return &Stop::instance; }

void Stop::run(Worker *worker, Packet packet) {
  UNUSED(packet);
  std::cout << "Stopping worker " << getpid() << std::endl;
  for (auto handle : worker->get_handles()) {
    Packet stop = WorkerProtocol::into(Stop::get_instance());
    handle.get_channel()->send(stop);
    handle.wait();
    handle.remove();
  }

  worker->stop();
}
