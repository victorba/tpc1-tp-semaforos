#include "model/work/build_grater.hpp"
#include "communication/packet.hpp"
#include "model/work/build_oven.hpp"
#include "model/work/stop.hpp"
#include "unused.hpp"
#include "worker/spawner.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>

BuildGrater BuildGrater::instance;

Work *BuildGrater::get_instance() { return &BuildGrater::instance; }

void BuildGrater::run(Worker *worker, Packet packet) {
  UNUSED(packet);
  std::cout << "Building grater in " << getpid() << std::endl;
  WorkerHandle handle = WorkerSpawner::spawn("oven.txt");
  if (handle.is_parent()) {
    worker->get_handles().push_back(handle);
    Packet build = WorkerProtocol::into(BuildOven::get_instance());
    handle.get_channel()->send(build);
  } else {
    worker->stop();
  }
}
