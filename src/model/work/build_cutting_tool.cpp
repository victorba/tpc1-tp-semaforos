#include "model/work/build_cutting_tool.hpp"
#include "communication/packet.hpp"
#include "model/work/build_grater.hpp"
#include "model/work/stop.hpp"
#include "unused.hpp"
#include "worker/spawner.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>

BuildCuttingTool BuildCuttingTool::instance;

Work *BuildCuttingTool::get_instance() { return &BuildCuttingTool::instance; }

void BuildCuttingTool::run(Worker *worker, Packet packet) {
  UNUSED(packet);
  std::cout << "Building cutting tool in " << getpid() << std::endl;
  WorkerHandle handle = WorkerSpawner::spawn("cheese.txt");
  if (handle.is_parent()) {
    worker->get_handles().push_back(handle);
    Packet build = WorkerProtocol::into(BuildGrater::get_instance());
    handle.get_channel()->send(build);
  } else {
    worker->stop();
  }
}
