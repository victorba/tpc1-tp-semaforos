#include "model/work/grate_cheese.hpp"
#include "communication/packet.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/shop/pizza.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

GrateCheese GrateCheese::instance;

Work *GrateCheese::get_instance() { return &GrateCheese::instance; }

void GrateCheese::run(Worker *worker, Packet packet) {
  Pizza pizza = PizzeriaProtocol::from(packet);
  pizza.add_cheese();
  std::cout << "Adding cheese in " << getpid() << " for pizza "
            << pizza.get_id() << std::endl;
  Packet request = PizzeriaProtocol::into_put_in_oven(pizza);
  worker->get_handles().front().get_channel()->send(request);
}
