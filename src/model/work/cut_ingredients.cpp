#include "model/work/cut_ingredients.hpp"
#include "communication/packet.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/shop/pizza.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

CutIngredients CutIngredients::instance;

Work *CutIngredients::get_instance() { return &CutIngredients::instance; }

void CutIngredients::run(Worker *worker, Packet packet) {
  Pizza pizza = PizzeriaProtocol::from(packet);
  pizza.add_ingredients();
  std::cout << "Adding ingredients in " << getpid() << " for pizza "
            << pizza.get_id() << std::endl;
  Packet request = PizzeriaProtocol::into_grate_cheese(pizza);
  worker->get_handles().front().get_channel()->send(request);
}
