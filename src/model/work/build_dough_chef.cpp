#include "model/work/build_dough_chef.hpp"
#include "communication/packet.hpp"
#include "model/work/build_cutting_tool.hpp"
#include "model/work/stop.hpp"
#include "unused.hpp"
#include "worker/spawner.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>

BuildDoughChef BuildDoughChef::instance;

Work *BuildDoughChef::get_instance() { return &BuildDoughChef::instance; }

void BuildDoughChef::run(Worker *worker, Packet packet) {
  UNUSED(packet);
  std::cout << "Building dough chef in " << getpid() << std::endl;
  WorkerHandle handle = WorkerSpawner::spawn("ingredients.txt");
  if (handle.is_parent()) {
    worker->get_handles().push_back(handle);
    Packet build = WorkerProtocol::into(BuildCuttingTool::get_instance());
    handle.get_channel()->send(build);
  } else {
    worker->stop();
  }
}
