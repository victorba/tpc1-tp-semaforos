#include "model/work/prepare_dough.hpp"
#include "communication/packet.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/work/cut_ingredients.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

PrepareDough PrepareDough::instance;

Work *PrepareDough::get_instance() { return &PrepareDough::instance; }

void PrepareDough::run(Worker *worker, Packet packet) {
  Pizza pizza = PizzeriaProtocol::from_order(packet);
  pizza.add_dough();
  std::cout << "Adding dough in " << getpid() << " for pizza " << pizza.get_id()
            << std::endl;
  Packet request = PizzeriaProtocol::into_cut_ingredients(pizza);
  worker->get_handles().front().get_channel()->send(request);
}
