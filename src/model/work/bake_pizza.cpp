#include "model/work/bake_pizza.hpp"
#include "communication/packet.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/shop/pizza.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

BakePizza BakePizza::instance;

Work *BakePizza::get_instance() { return &BakePizza::instance; }

void BakePizza::run(Worker *worker, Packet packet) {
  UNUSED(worker);
  Pizza pizza = PizzeriaProtocol::from(packet);
  int time = pizza.bake();
  std::cout << "Baking in chamber " << getpid() << " for pizza "
            << pizza.get_id() << " for " << time << " miliseconds" << std::endl;
}
