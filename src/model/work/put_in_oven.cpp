#include "model/work/put_in_oven.hpp"
#include "communication/packet.hpp"
#include "model/pizzeria_protocol.hpp"
#include "model/shop/pizza.hpp"
#include "unused.hpp"
#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

PutInOven PutInOven::instance;

Work *PutInOven::get_instance() { return &PutInOven::instance; }

void PutInOven::run(Worker *worker, Packet packet) {
  Pizza pizza = PizzeriaProtocol::from(packet);
  std::cout << "Putting in oven in " << getpid() << " for pizza "
            << pizza.get_id() << std::endl;

  const int MAX_CHAMBERS = worker->get_integers()["MAX CHAMBERS"];
  int chamber = worker->get_integers()["chamber"];
  chamber = (chamber + 1) % MAX_CHAMBERS;
  worker->get_integers()["chamber"] = chamber;
  WorkerHandle &handle = worker->get_handles()[chamber];

  Packet request = PizzeriaProtocol::into_bake_pizza(pizza);
  handle.get_channel()->send(request);
}
