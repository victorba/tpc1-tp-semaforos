#include "model/work/build_chamber.hpp"
#include "communication/packet.hpp"
#include "model/work/build_grater.hpp"
#include "model/work/stop.hpp"
#include "unused.hpp"
#include "worker/spawner.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_spawner.hpp"
#include <iostream>
#include <sstream>

BuildChamber BuildChamber::instance;

Work *BuildChamber::get_instance() { return &BuildChamber::instance; }

void BuildChamber::run(Worker *worker, Packet packet) {
  UNUSED(worker);
  UNUSED(packet);
  std::cout << "Building chamber in " << getpid() << std::endl;
}
