#include "worker/worker_handle.hpp"
#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>

WorkerHandle::WorkerHandle(const std::string pathname, pid_t pid,
                           Channel<Packet> channel)
    : pathname(pathname), pid(pid), channel(channel) {}

void WorkerHandle::remove() {
  std::remove(this->pathname.c_str());
  this->channel.remove();
}

int WorkerHandle::wait() const {
  int wstatus;
  waitpid(this->pid, &wstatus, 0);
  return WEXITSTATUS(wstatus);
}

Channel<Packet> *WorkerHandle::get_channel() { return &this->channel; }

bool WorkerHandle::is_parent() const { return this->pid != 0; }

pid_t WorkerHandle::getpid() const { return this->pid; }
