#include "worker/work_serializer.hpp"

WorkSerializer WorkSerializer::instance;

WorkSerializer *WorkSerializer::get_instance() {
  return &WorkSerializer::instance;
}

void WorkSerializer::add(Work *work) { this->works.push_back(work); }

int WorkSerializer::into(Work *work) const {
  for (unsigned int i = 0; i < this->works.size(); i++) {
    if (this->works[i] == work) {
      return i;
    }
  }
  return -1;
}

Work *WorkSerializer::from(int workid) const { return this->works[workid]; }
