#include "worker/worker_protocol.hpp"
#include "worker/work_serializer.hpp"

Packet WorkerProtocol::into(Work *work) {
  Packet packet;
  packet.header.work = WorkSerializer::get_instance()->into(work);
  return packet;
}

Work *WorkerProtocol::from(Packet packet) {
  int workid = packet.header.work;
  return WorkSerializer::get_instance()->from(workid);
}
