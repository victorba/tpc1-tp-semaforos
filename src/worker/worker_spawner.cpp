#include "worker/worker_spawner.hpp"

WorkerHandle WorkerSpawner::spawn(const std::string &pathname) {
  std::fstream file(pathname, std::ios::out);
  Channel<Packet> channel(pathname);
  pid_t pid = WorkerSpawner::fork(channel);
  WorkerHandle handle(pathname, pid, channel);
  return handle;
}

pid_t WorkerSpawner::fork(Channel<Packet> channel) {
  pid_t pid;

  pid = ::fork();
  if (pid == 0) {
    Worker worker(channel);
    worker.run();
  }
  return pid;
}
