#include "worker/worker.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

Worker::Worker(Channel<Packet> channel)
    : channel(channel), keep_running(true) {}

Worker::~Worker() { this->channel.free(); }

void Worker::run() {
  while (this->keep_running) {
    Packet packet = this->channel.recv();
    Work *work = WorkerProtocol::from(packet);
    work->run(this, packet);
  }
}

void Worker::stop() { this->keep_running = false; }

Channel<Packet> &Worker::get_channel() { return this->channel; }

std::vector<WorkerHandle> &Worker::get_handles() { return this->handles; }

std::map<std::string, int> &Worker::get_integers() { return this->integers; }
