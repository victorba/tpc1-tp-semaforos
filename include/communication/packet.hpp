#ifndef __PACKET_HPP__
#define __PACKET_HPP__

#include "communication/body.hpp"
#include "communication/header.hpp"

class Packet {
public:
  Header header;
  Body body;
};

#endif
