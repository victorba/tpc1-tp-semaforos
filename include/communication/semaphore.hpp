#ifndef __SEMAPHORE_HPP__
#define __SEMAPHORE_HPP__

#include <string>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>

class Semaphore {
private:
  int semid;

public:
  Semaphore(const std::string &pathname, int projid, int count);
  ~Semaphore();
  int acquire() const;
  int release() const;
  void remove() const;

private:
  void initialize(int count) const;
};

#endif
