#ifndef __SHARED_MEMORY_HPP__
#define __SHARED_MEMORY_HPP__

#include "exception/exception.hpp"
#include <sstream>
#include <stdexcept>
#include <string.h>
#include <string>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <unistd.h>

template <class T> class SharedMemory {
private:
  int shmid;
  T *ptr;

public:
  SharedMemory(const std::string &pathname, int projid);
  ~SharedMemory();
  void write(const T &data);
  T read() const;
  T *get() const;
  void free() const;
  void detach() const;
  int attach_count() const;

private:
  void remove() const;
};

template <class T>
SharedMemory<T>::SharedMemory(const std::string &pathname, int projid) {
  key_t key = ftok(pathname.c_str(), projid);
  if (key != -1) {
    this->shmid = shmget(key, sizeof(T), 0644 | IPC_CREAT);
    if (this->shmid != -1) {
      this->ptr = (T *)shmat(this->shmid, NULL, 0);
      if (this->ptr == (void *)-1) {
        throw Exception("SharedMemory error in shmat");
      }
    } else {
      throw Exception("SharedMemory error in shmget");
    }
  } else {
    throw Exception("SharedMemory error in ftok");
  }
}

template <class T> SharedMemory<T>::~SharedMemory() {}

template <class T> void SharedMemory<T>::write(const T &data) {
  *(this->ptr) = data;
}

template <class T> T SharedMemory<T>::read() const { return *(this->ptr); }

template <class T> T *SharedMemory<T>::get() const { return this->ptr; }

template <class T> void SharedMemory<T>::free() const {
  this->detach();
  if (this->attach_count() == 0) {
    this->remove();
  }
}

template <class T> void SharedMemory<T>::detach() const {
  if (shmdt(static_cast<void *>(this->ptr)) == -1) {
    throw Exception("SharedMemory error in detach");
  }
}

template <class T> int SharedMemory<T>::attach_count() const {
  shmid_ds status;
  if (shmctl(this->shmid, IPC_STAT, &status) == -1) {
    throw Exception("SharedMemory error in attach count");
  }
  return status.shm_nattch;
}

template <class T> void SharedMemory<T>::remove() const {
  if (shmctl(this->shmid, IPC_RMID, NULL) == -1) {
    throw Exception("SharedMemory error in remove");
  }
}

#endif
