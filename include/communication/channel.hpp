#ifndef __CHANNEL_HPP__
#define __CHANNEL_HPP__

#include "communication/circular_queue.hpp"
#include "communication/semaphore.hpp"
#include "communication/shared_memory.hpp"
#include <string>

template <class T> class Channel {
private:
  SharedMemory<CircularQueue<T>> queue;
  Semaphore mutex;
  Semaphore items;
  Semaphore spaces;

public:
  Channel(const std::string &pathname);
  void send(T item);
  T recv();
  void free();
  void remove();
  int attach_count() const;
};

template <class T>
Channel<T>::Channel(const std::string &pathname)
    : queue(pathname, 'a'), mutex(pathname, 'b', 1), items(pathname, 'c', 0),
      spaces(pathname, 'd', CircularQueue<T>::capacity()) {
  CircularQueue<T> other_queue;
  this->queue.write(other_queue);
}

template <class T> void Channel<T>::send(T item) {
  this->spaces.acquire();
  this->mutex.acquire();
  this->queue.get()->enqueue(item);
  this->mutex.release();
  this->items.release();
}

template <class T> T Channel<T>::recv() {
  this->items.acquire();
  this->mutex.acquire();
  T item = this->queue.get()->dequeue();
  this->mutex.release();
  this->spaces.release();
  return item;
}

template <class T> void Channel<T>::free() { this->queue.free(); }

template <class T> void Channel<T>::remove() {
  this->free();
  this->mutex.remove();
  this->items.remove();
  this->spaces.remove();
}

template <class T> int Channel<T>::attach_count() const {
  return this->queue.attach_count();
}

#endif
