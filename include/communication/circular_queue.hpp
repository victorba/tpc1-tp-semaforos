#ifndef __CIRCULAR_QUEUE_HPP__
#define __CIRCULAR_QUEUE_HPP__

#include "exception/exception.hpp"
#include <stdexcept>

#define CQ_MAX_SIZE 10

template <class T> class CircularQueue {
private:
  int head;
  int tail;
  unsigned int sz;
  T data[CQ_MAX_SIZE];

public:
  CircularQueue();
  bool is_empty() const;
  bool is_full() const;
  unsigned int size() const;
  void enqueue(T item);
  T dequeue();
  static unsigned int capacity();
};

template <class T>
CircularQueue<T>::CircularQueue() : head(0), tail(-1), sz(0) {}

template <class T> bool CircularQueue<T>::is_empty() const {
  return this->size() == 0;
}

template <class T> bool CircularQueue<T>::is_full() const {
  return this->size() == CircularQueue<T>::capacity();
}

template <class T> unsigned int CircularQueue<T>::size() const {
  return this->sz;
}

template <class T> void CircularQueue<T>::enqueue(T item) {
  if (this->is_full()) {
    throw Exception("Error enqueue: CircularQueue is full");
  }
  this->tail = (this->tail + 1) % CircularQueue<T>::capacity();
  this->data[this->tail] = item;
  this->sz++;
}

template <class T> T CircularQueue<T>::dequeue() {
  if (this->is_empty()) {
    throw Exception("Error dequeue: CircularQueue is empty");
  }
  T item = this->data[this->head];
  this->head = (this->head + 1) % CircularQueue<T>::capacity();
  this->sz--;
  return item;
}

template <class T> unsigned int CircularQueue<T>::capacity() {
  return CQ_MAX_SIZE;
}

#endif
