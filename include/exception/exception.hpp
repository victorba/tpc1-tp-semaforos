#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <sstream>
#include <stdexcept>
#include <string.h>
#include <string>

class Exception : public std::runtime_error {
public:
  Exception(const std::string &message);

private:
  std::string format(const std::string &message) const;
};

#endif
