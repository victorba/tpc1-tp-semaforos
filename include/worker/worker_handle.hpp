#ifndef __WORKER_HANDLE_HPP__
#define __WORKER_HANDLE_HPP__

#include "communication/channel.hpp"
#include "communication/packet.hpp"
#include <string>

class WorkerHandle {
private:
  std::string pathname;
  pid_t pid;
  Channel<Packet> channel;

public:
  WorkerHandle(const std::string pathname, pid_t pid, Channel<Packet> channel);
  int wait() const;
  Channel<Packet> *get_channel();
  bool is_parent() const;
  void remove();
  pid_t getpid() const;
};

#endif
