#ifndef __WORKER_HPP__
#define __WORKER_HPP__

#include "communication/channel.hpp"
#include "communication/packet.hpp"
#include "worker/work.hpp"
#include "worker/worker_handle.hpp"
#include "worker/worker_protocol.hpp"
#include <map>
#include <string>
#include <vector>

class Worker {
private:
  Channel<Packet> channel;
  bool keep_running;
  std::vector<WorkerHandle> handles;
  std::map<std::string, int> integers;

public:
  Worker(Channel<Packet> channel);
  ~Worker();
  void run();
  void stop();
  Channel<Packet> &get_channel();
  std::vector<WorkerHandle> &get_handles();
  std::map<std::string, int> &get_integers();
};

#endif
