#ifndef __WORKER_SPAWNER_HPP__
#define __WORKER_SPAWNER_HPP__

#include "communication/channel.hpp"
#include "communication/packet.hpp"
#include "worker/worker.hpp"
#include "worker/worker_handle.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <sys/types.h>
#include <unistd.h>

class WorkerSpawner {
public:
  static WorkerHandle spawn(const std::string &pathname);

private:
  static pid_t fork(Channel<Packet> channel);
};

#endif
