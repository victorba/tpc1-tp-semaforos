#ifndef __SPAWNER_HPP__
#define __SPAWNER_HPP__

#include "communication/channel.hpp"
#include "communication/packet.hpp"
#include <sys/types.h>
#include <unistd.h>

template <class T> class Spawner {
public:
  static pid_t spawn(Channel<Packet> channel);
};

template <class T> pid_t Spawner<T>::spawn(Channel<Packet> channel) {
  pid_t pid;

  pid = fork();
  if (pid == 0) {
    T t(channel);
    t.run();
  }
  return pid;
}

#endif
