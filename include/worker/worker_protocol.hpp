#ifndef __WORKER_PROTOCOL_HPP__
#define __WORKER_PROTOCOL_HPP__

#include "communication/packet.hpp"

class Work;

class WorkerProtocol {
public:
  static Packet into(Work *work);
  static Work *from(Packet packet);
};

#endif
