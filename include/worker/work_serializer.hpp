#ifndef __WORK_SERIALIZER_HPP__
#define __WORK_SERIALIZER_HPP__

#include <vector>

class Work;

class WorkSerializer {
private:
  static WorkSerializer instance;
  std::vector<Work *> works;

public:
  static WorkSerializer *get_instance();
  void add(Work *work);
  int into(Work *work) const;
  Work *from(int workid) const;
};

#endif
