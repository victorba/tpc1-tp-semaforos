#ifndef __WORK_HPP__
#define __WORK_HPP__

#include "communication/packet.hpp"

class Worker;

class Work {
public:
  virtual void run(Worker *worker, Packet packet) = 0;
};

#endif
