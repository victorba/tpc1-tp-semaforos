#ifndef __PIZZERIA_PROTOCOL_HPP__
#define __PIZZERIA_PROTOCOL_HPP__

#include "communication/packet.hpp"
#include "model/shop/pizza.hpp"

class Work;

class PizzeriaProtocol {
public:
  static Pizza from(Packet packet);
  static Pizza from_order(Packet packet);
  static Packet into_prepare_dough(int id);
  static Packet into_cut_ingredients(const Pizza &pizza);
  static Packet into_grate_cheese(const Pizza &pizza);
  static Packet into_put_in_oven(const Pizza &pizza);
  static Packet into_bake_pizza(const Pizza &pizza);

private:
  static Packet into_packet(const Pizza &pizza, Work *work);
};

#endif
