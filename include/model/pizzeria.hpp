#ifndef __PIZZERIA_HPP__
#define __PIZZERIA_HPP__

class Pizzeria {
private:
  const int ORDERS_INDEX = 1;

public:
  Pizzeria();
  void run(int argc, char *argv[]) const;

private:
  int get_orders(int argc, char *argv[]) const;
  void load_works() const;
};

#endif
