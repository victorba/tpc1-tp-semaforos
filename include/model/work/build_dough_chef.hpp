#ifndef __BUILD_DOUGH_CHEF_HPP__
#define __BUILD_DOUGH_CHEF_HPP__

#include "worker/work.hpp"

class BuildDoughChef : public Work {
private:
  static BuildDoughChef instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
