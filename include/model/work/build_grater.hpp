#ifndef __BUILD_GRATER_HPP__
#define __BUILD_GRATER_HPP__

#include "worker/work.hpp"

class BuildGrater : public Work {
private:
  static BuildGrater instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
