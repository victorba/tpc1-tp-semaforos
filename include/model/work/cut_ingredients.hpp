#ifndef __CUT_INGREDIENTS_HPP__
#define __CUT_INGREDIENTS_HPP__

#include "worker/work.hpp"

class CutIngredients : public Work {
private:
  static CutIngredients instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
