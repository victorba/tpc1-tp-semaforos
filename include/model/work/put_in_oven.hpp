#ifndef __PUT_IN_OVEN_HPP__
#define __PUT_IN_OVEN_HPP__

#include "worker/work.hpp"

class PutInOven : public Work {
private:
  static PutInOven instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
