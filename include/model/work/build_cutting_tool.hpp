#ifndef __BUILD_INGREDIENTS_CHEF_HPP__
#define __BUILD_INGREDIENTS_CHEF_HPP__

#include "worker/work.hpp"

class BuildCuttingTool : public Work {
private:
  static BuildCuttingTool instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
