#ifndef __PREPARE_DOUGH_HPP__
#define __PREPARE_DOUGH_HPP__

#include "worker/work.hpp"

class PrepareDough : public Work {
private:
  static PrepareDough instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
