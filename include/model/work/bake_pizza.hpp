#ifndef __BAKE_PIZZA_HPP__
#define __BAKE_PIZZA_HPP__

#include "worker/work.hpp"

class BakePizza : public Work {
private:
  static BakePizza instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
