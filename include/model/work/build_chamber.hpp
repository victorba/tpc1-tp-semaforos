#ifndef __BUILD_CHAMBER_HPP__
#define __BUILD_CHAMBER_HPP__

#include "worker/work.hpp"

class BuildChamber : public Work {
private:
  static BuildChamber instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
