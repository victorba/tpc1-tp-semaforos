#ifndef __STOP_HPP__
#define __STOP_HPP__

#include "worker/work.hpp"

class Stop : public Work {
private:
  static Stop instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
