#ifndef __BUILD_OVEN_HPP__
#define __BUILD_OVEN_HPP__

#include "worker/work.hpp"
#include "worker/worker_handle.hpp"

class BuildOven : public Work {
private:
  static BuildOven instance;
  const int MAX_CHAMBERS = 5;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);

private:
  void free_handles(Worker *worker, WorkerHandle &handle) const;
};

#endif
