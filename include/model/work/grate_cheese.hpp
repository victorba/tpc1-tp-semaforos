#ifndef __GRATE_CHEESE_HPP__
#define __GRATE_CHEESE_HPP__

#include "worker/work.hpp"

class GrateCheese : public Work {
private:
  static GrateCheese instance;

public:
  static Work *get_instance();
  virtual void run(Worker *worker, Packet packet);
};

#endif
