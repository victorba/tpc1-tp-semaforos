#ifndef __PIZZA_HPP__
#define __PIZZA_HPP__

enum class PIZZA_STATE : int { EMPTY = 0, ONLY_DOUGH, SEASONED, RAW, BAKED };

class Pizza {
private:
  int id;
  PIZZA_STATE state;

public:
  Pizza(int id);
  Pizza(int id, PIZZA_STATE state);
  int get_id() const;
  PIZZA_STATE get_state() const;
  void add_dough();
  void add_ingredients();
  void add_cheese();
  int bake();
};

#endif
