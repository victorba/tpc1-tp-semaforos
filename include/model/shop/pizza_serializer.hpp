#ifndef __PIZZA_SERIALIZER_HPP__
#define __PIZZA_SERIALIZER_HPP__

#include "communication/packet.hpp"
#include "model/shop/pizza.hpp"

class PizzaSerializer {
public:
  static void into(const Pizza &pizza, Packet &packet);
  static Pizza from(int id, int state);
  static Pizza from_order(Packet &packet);
};

#endif
