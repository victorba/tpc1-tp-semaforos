# tpc1-tp-semaforos

# TP Especial Semaforos - Tecnicas de Programacion Concurrente I
Autor: Victor Bravo

## Compilacion
```
mkdir build
cd build
cmake ..
```

## Ejecucion
```
./pizzeria <cantidad de ordenes>
```

## Ejemplo
```
./pizzeria 10
```
